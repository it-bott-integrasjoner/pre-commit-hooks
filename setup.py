from setuptools import find_packages, setup

setup(
    name="bottint_pre_commit_hooks",
    version="0.0.0",
    packages=find_packages("."),
    entry_points={
        "console_scripts": [
            "detect-api-key = pre_commit_hooks.detect_api_key:main",
        ],
    },
)
