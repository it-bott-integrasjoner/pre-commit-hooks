import string
from sys import argv


def is_target_line(line: str) -> bool:
    """Checks if a string contains the name of an api key"""
    lower_line = line.lower()
    contains_api_key_keyword = "api" in lower_line and "key" in lower_line

    predefined_key_names = []
    contains_predefined_keyword = any([name in line for name in predefined_key_names])

    return contains_api_key_keyword or contains_predefined_keyword


def should_ignore_line(line: str) -> bool:
    return bool("detect-api-key: ignore" in line)


def extract_value(line: str) -> str:
    api_key = ""
    if ":" in line:
        api_key_split = line.strip().split(":")
        api_key = (
            "".join(api_key_split[1:]) if len(api_key_split) > 1 else api_key_split[0]
        )
    elif "=" in line:
        api_key_split = line.strip().split("=")
        api_key = (
            "".join(api_key_split[1:]) if len(api_key_split) > 1 else api_key_split[0]
        )
    return api_key


def strip_to_raw(api_key: str) -> str:
    api_key_raw = api_key.strip(" #")
    if "#" in api_key_raw:
        api_key_raw = api_key_raw.split("#")[0]
    api_key_raw = (
        api_key_raw.replace("'", "")
        .replace('"', "")
        .replace("{", "")
        .replace("}", "")
        .replace(",", "")
        .replace("-", "")
        .strip()
    )
    return api_key_raw


def has_api_key_format(api_key: str) -> bool:
    min_api_key_length = 24
    has_required_length = len(api_key) >= min_api_key_length

    api_key_characters = string.ascii_letters + string.digits
    only_contains_api_chars = all([letter in api_key_characters for letter in api_key])

    return has_required_length and only_contains_api_chars


def check_file(file_name: str) -> bool:
    found_key = False

    with open(file_name, "r") as file:
        for line_number, line in enumerate(file):
            if not is_target_line(line):
                continue
            if should_ignore_line(line):
                continue
            api_key = extract_value(line)
            api_key_raw = strip_to_raw(api_key)
            if not has_api_key_format(api_key_raw):
                continue
            print(
                f"The file {file_name} may contain an api-key on line {line_number+1}."
            )
            found_key = True

    return found_key


def main() -> int:
    file_names = argv[1:]
    found_key = []

    for file_name in file_names:
        try:
            found_key.append(check_file(file_name))
        except UnicodeDecodeError:
            print(f"Failed to read file {file_name}")

    if any(found_key):
        print("\nOne may ignore a line by commenting 'detect-api-key: ignore'")
        print("\nIf you wish to commit anyway, run:")
        print("git commit --no-verify\n")
        return 1
    return 0


if __name__ == "__main__":
    raise SystemExit(main())
